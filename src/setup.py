#! /usr/bin/env python3

from setuptools import setup

setup(
    name='Magnify',
    version='0.0.0',
    author='',
    packages=[
        'magnify',
        'magnify.api',
        'magnify.api.accounts'
    ],
    license="GNU/GPLv3",
    platforms="Linux",
    classifiers=[
        "Environment :: Console",
        "Development Status :: 5 - Production/Stable",
        "Programming Language :: Python :: 3"
    ],
    install_requires=['httpx'],
    scripts=[]
)
