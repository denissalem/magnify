#! /usr/bin/env python3

import datetime
import json
import os

def update_fediverse_nodes(force=False):
    '''Save known fediverse nodes as JSON file from https://api.fediverse.observer.
    By default it will not perform any download unless the destination file doesn't exist or is older than twelve hours.'''

    if not os.path.isdir(os.path.expanduser("~/.magnify")):
        os.mkdir(os.path.expanduser("~/.magnify"))
  
    isnotfile = not os.path.isfile(os.path.expanduser("~/.magnify/fediverse.json"))
    isoutdated = isnotfile or datetime.datetime.now().timestamp() - os.path.getmtime(os.path.expanduser("~/.magnify/fediverse.json")) > 43200
    isempty = isnotfile or os.stat(os.path.expanduser("~/.magnify/fediverse.json")).st_size == 0
    if force or isnotfile or isoutdated or isempty:
        with open(os.path.expanduser("~/.magnify/fediverse.json"), "wb") as f:
            from urllib.request import Request, urlopen
            request = Request(
                "https://api.fediverse.observer",
                data=b'{"query": "query {nodes { masterversion shortversion softwarename domain} }"}',
                headers={"Content-Type": "application/json"}
            )
            with urlopen(request) as stream:
                f.write(stream.read())

def yield_fediverse_nodes():
    '''Yield each known cached node from fediverse.
    If cache doesn't exists or is outdated, it is automatically created.'''
    
    try:
        update_fediverse_nodes()
        
    except Exception as e:
        pass # TODO log error
        
    with open(os.path.expanduser("~/.magnify/fediverse.json"), "r") as f:
      data = json.load(f)
      for node in data["data"]["nodes"]:
          yield node
