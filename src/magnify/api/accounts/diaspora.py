#! /usr/bin/env python3 

import httpx

from magnify.api.accounts import exception_handler
from magnify.api.accounts import TIMEOUT

async def __diaspora(service, username, callback, semaphore, pool_limits):
    async with semaphore:
        account_url = "https://"+service["domain"]+'/u/'+username
        async with httpx.AsyncClient(limits=pool_limits) as client:
            try:
                r = await client.get(account_url, timeout=httpx.Timeout(TIMEOUT))
                http_code = r.status_code
                if http_code != 404:
                    callback({
                        "http_code" : http_code,
                        "status" : "Match" if http_code == 200 else "Undefined",
                        "account_url": account_url,
                        "exception": None
                    })
                
            except Exception as e:
                exception_handler(e, callback, account_url)
