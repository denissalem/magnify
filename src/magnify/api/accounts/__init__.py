#! /usr/bin/env python3

import asyncio

import httpx

from magnify.api.nodes import yield_fediverse_nodes

# TODO Should be user defined
POOL_SIZE = 512
TIMEOUT = 10.0

async def __unknown_service(service, username, callback, semaphore, pool_limits):
    pass # TODO log error

def __test_service(service, username, callback, semaphore, pool_limits):
    if service["softwarename"] == "diaspora":
        from magnify.api.accounts.diaspora import __diaspora
        worker = __diaspora
    else:
        worker = __unknown_service
        
    return worker(service, username, callback, semaphore, pool_limits)

def __yield_services_and_usernames(usernames):
    for username in usernames:
        for service in yield_fediverse_nodes():
            yield service, username

def exception_handler(e, callback, account_url):
  types = [
      httpx.ReadTimeout,
      httpx.ConnectError,
      httpx.ConnectTimeout
  ]
  if type(e) in types:
      callback({
          "http_code" : None,
          "status" : "Undefined",
          "account_url": account_url,
          "exception": e
      })
  else:
      print(e)
      pass # TODO log error
        
async def get_accounts(usernames, callback):
    '''Get accounts asynchroneously from known services against given list of usernames.
    Accounts are passed individually to callback so end user can update it's data from its side.'''

    semaphore = asyncio.Semaphore(POOL_SIZE)
    pool_limits = httpx.Limits(max_keepalive_connections=POOL_SIZE, max_connections=POOL_SIZE)
    await asyncio.gather(*[
        __test_service(*service_username, callback, semaphore, pool_limits) for service_username in __yield_services_and_usernames(usernames)
    ])
    
    
