#!/usr/bin/env python3

import asyncio

from magnify.api.accounts import get_accounts

def process(account):
    if account["status"] == "Match":
        print(account)


asyncio.run(get_accounts(['denissalem'], process))
